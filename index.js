/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
//first function here:

	function inputPersonalDetails(){
		let fullName = prompt("Enter your Full Name: ");
		let age = prompt("Enter your Age: ");
		let location = prompt("Enter your Location: ");

			console.log("Hello, " +fullName);
			console.log("You are " +age+ " years old.");
			console.log("You live in " +location);
		}
		
		inputPersonalDetails();


/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/

//second function here:

	function myTopFavoriteBands() {
		console.log("1. The Beatles ");
		console.log("2. Metallica ");
		console.log("3. The Eagles ");
		console.log("4. L'arc~en~Ciel ");
		console.log("5. Earaserheads ");
	}	
	
	myTopFavoriteBands();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
//third function here:

	function myTopFavoriteMovies(){
		let firstFavoriteMovie = "1. The Godfather ";
		let secondFavoriteMovie = "2. The Godfather, Part II ";
		let thirdFavoriteMovie = "3. Shawshank Redemption ";
		let fourthFavoriteMovie = "4. To Kill A Mockingbird ";
		let fifthFavoriteMovie = "5. Psycho ";

		console.log(firstFavoriteMovie);
		console.log("Rotten Tomatoes Rating : 97% ")
		console.log(secondFavoriteMovie);
		console.log("Rotten Tomatoes Rating : 96% ")
		console.log(thirdFavoriteMovie);
		console.log("Rotten Tomatoes Rating : 91% ")
		console.log(fourthFavoriteMovie);
		console.log("Rotten Tomatoes Rating : 93% ")
		console.log(fifthFavoriteMovie);
		console.log("Rotten Tomatoes Rating : 96% ")
	}	

	myTopFavoriteMovies();


/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

function printFriends(){
	alert("Hi! Please add the names of your friends.");
	let firstFriendName = prompt("Enter your first friend's name:"); 
	let secondFriendName = prompt("Enter your second friend's name:"); 
	let thirdFriendName = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(firstFriendName); 
	console.log(secondFriendName); 
	console.log(thirdFriendName); 
}
	printFriends();

